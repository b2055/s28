//JAVASCRIPT SYNCHRONUS VS ASYNCHRONUS

//SYNCHRONUS JAVASCRIPT
//in synchronus operations taska are performed one at a time and only when one is completed, the following is unblocked
//javascript is by default is synchronus
console.log("Hello world")
console.log("Hello Again")

for (let i=0; i <= 15; i++) {
    console.log(i)
}

//blocking is when the execution of additional js process must wait until the operation completes
//blocking is just the code is slow
console.log("Hello Goodbye")

//asynchronus javascript
//we can classify most asynchronus js operations with 2 primary triggers

//1. Browser API/WEB API events or functions. These include methods like setTimeout, or event handlers like onlick, mouse over scroll and many more

function printMe() {
    console.log("print ME")
}

setTimeout(printMe, 5000)

//another example

function print() {
    console.log("print mee")

}

function test() {
    console.log("test")
}

setTimeout(print, 4000)
test()

//another example

function f1() {
    console.log("f1")
}

function f2() {
    console.log("f2")
}

function main() {
    console.log("main")

    setTimeout(f1, 0)

    new Promise((resolve, reject) =>
        resolve("I am a promise")
    ).then(resolve => console.log(resolve))
    
    f2()
}

main()

//callback queue (data structure)
//in javascript, we have a queue of callback functions. It is called a callback queue or task queue
//A queue data structure is "first-in-first-out"

//job queue
//everytime a promise occurs in the code, the executor function gets into the job queue. The event loop works as usual, to look into the queues but gives priority to the job queues
//items over the callback queue items when the stack is free.

//2. Promises - A unique javascrupt object that allows us to perform asynchronus

//The promises object represents the eventual completion or failure of an asynchronus operation and its resulting value

/* new Promise((resolve, reject) =>
    resolve("I am a promise")
).then(resolve => console.log(resolve)) */

//REST API using a jsonplaceholder

//Getting all posts (GET method)

//Fetch -> allows us to asynchronously request for a resource(data)
// we used the promise for async


/* 
    syntax:

        fetch('url', {optional objects})
        .then(response => response.json())
        .then(data)
*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
//A promise may be in one of 3 possible states: pending, fulfilled or rejected.
//parse the response as JSON
//process the results
fetch('https://jsonplaceholder.typicode.com/posts', {method: 'GET'}).then(response => response.json()).then(data => {console.log(data)})

 //async await function

 async function fetchData(){
     //wait for the "fetch" method to complete then stores the value in a variable name
     let result = await fetch('https://jsonplaceholder.typicode.com/posts')
     console.log(result)
     console.log(typeof result)

     //converts the data from the "response"
     let json = await result.json()
     console.log(json)
     console.log(typeof json)
 }

 fetchData()

 fetch('https://jsonplaceholder.typicode.com/posts/1').then(response => response.json()).then(data => {console.log(data) })

 //creates a new post

fetch('https://jsonplaceholder.typicode.com/posts', { 
    method:'POST',
    headers: {
        'Content-Type':'application/json'
    },
    //Sets the content/body of the 'request' object to be sent to the backend
    body: JSON.stringify({
        title: 'New Post',
        body: 'Hello Again',
        userId: 2
    })
})
.then(response => response.json())
.then(data => {console.log(data) })


fetch('https://jsonplaceholder.typicode.com/posts/1', { 
    method:'PUT',
    headers: {
        'Content-Type':'application/json'
    },
    //Sets the content/body of the 'request' object to be sent to the backend
    body: JSON.stringify({
        title: 'New Revised',
        body: 'Revised',
        userId: 1
    })
})
.then(response => response.json())
.then(data => {console.log(data) })

//Put method - updates the whole document. modifyiing resource where the client sends data that updates the entire resource

//The patch method applies partial update to resource

//Delete a post

fetch('https://jsonplaceholder.typicode.com/posts/5', { method:'DELETE' })
.then(response => response.json())
.then(data => {console.log(data) })


//Filtering posts

/* 
    syntax:
    //individual parameters
        //'url?parameterName=value'
    //Multiple parameters
        //'url?parameterName=value&paraB=valueB'
*/
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(response => response.json())
.then(data => {console.log(data) })

//Retrieve a nested/related comments to posts
//retrieving comments for a specific post(/post/:id/comments)

fetch('https://jsonplaceholder.typicode.com/posts/2/comments')
.then(response => response.json())
.then(data => {console.log(data) })