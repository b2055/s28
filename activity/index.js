//3
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
    console.log(data) 

//4
   let mapList = data.map(title => ({title:title.title}))
   console.log(mapList)
   
})

//5
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(data => {
    console.log(data) 

//6
    console.log(`To Do List #1`)
    console.log(`title:${data.title}, status completion:${data.completed}`)
   
})

//7
fetch('https://jsonplaceholder.typicode.com/todos', { 
    method:'POST',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        title: 'POST TITLE',
        description: 'DESCRIPTION FOR POST',
        completed: true,
        dateCompleted: "07/12/2012",
        userId: 1
    })
})
.then(response => response.json())
.then(data => {console.log(data)})


//8
//9
fetch('https://jsonplaceholder.typicode.com/todos/1', { 
    method:'PUT',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        title: 'PUT TITLE',
        description: 'DESCRIPTION FOR PUT',
        completed: true,
        dateCompleted: "07/12/1999",
        userId: 2
    })
})
.then(response => response.json())
.then(data => {console.log(data) })

//10
//11
fetch('https://jsonplaceholder.typicode.com/todos/6', { 
    method:'PATCH',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        completed: true,
        dateCompleted: "03/12/2021",
    })
})
.then(response => response.json())
.then(data => {console.log(data) })

//12
fetch('https://jsonplaceholder.typicode.com/todos/5', { method:'DELETE' })
.then(response => response.json())
.then(data => {console.log(data) })




